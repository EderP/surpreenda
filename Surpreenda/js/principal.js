//Escolher cidade
function escolhaCidade(atual){
    var select = document.getElementById("citySelect");
    var option = select.options[select.selectedIndex].value;

    if(option == atual)
    {
        return;
    }

    if(option == 0){

    }
    else if(option == 1){
        location.replace('../html/uberlandia.html');
    }
}

//Configurações iniciais páginas
function loadScreen(numero){
    var select = document.getElementById("citySelect");
    var option = select.options[select.selectedIndex].value;

    select.value = numero;
}

//Navegação entre páginas
function navegarPags(pag){
    if(pag == 0){
        location.replace('../html/index.html');
    }
    if(pag == 2){
        location.replace('../html/cadastro.html');
    }
}


//SlideShow
var slideIndex = [1,1];
var slideId = ["mySlides1", "mySlides2"]
showSlides(1, 0);
showSlides(1, 1);

function plusSlides(n, no) {
    showSlides(slideIndex[no] += n, no);
}

function showSlides(n, no) {
    var i;
    var x = document.getElementsByClassName(slideId[no]);
    if (n > x.length) {slideIndex[no] = 1}
    if (n < 1) {slideIndex[no] = x.length}
    for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
    }
    x[slideIndex[no]-1].style.display = "block";
}
